module.exports = {
    root: true,
    parser: 'babel-eslint',
    parserOptions: {
        sourceType: 'module'
    },
    extends: 'airbnb-base',
    // required to lint *.vue files
    plugins: [
        'html'
    ],
    // check if imports actually resolve
    'settings': {
        'import/resolver': {
            'webpack': {
                'config': 'build/webpack.base.conf.js'
            }
        }
    },
    // add your custom rules here
    'rules': {
        "semi": [1, "never"],
        'linebreak-style': [0, 'unix'],
        "indent": ["error", 4],
        "comma-dangle": ["error", "never"],
        "func-names": ["error", "never"],
        "space-before-blocks": [0, "never"],
        "padded-blocks": [0, "always"],
        "arrow-body-style": [0, "as-needed"],
        "global-require": [0],
        "import/no-dynamic-require": [0],
        "import/newline-after-import": [0],
        "prefer-const": [0],
        "no-plusplus": [0],
        "no-unused-vars": [0],
        "space-before-function-paren": [0],
        "no-shadow": [0],
        "no-param-reassign": [0],
        "object-curly-spacing": [0],
        "no-unused-expressions": [0],
        "eol-last": [0],
        "no-console": [0],
        "arrow-parens": [0],
        "no-trailing-spaces": [0],
        "no-undef": [0],
        "no-empty": [0],
        // don't require .vue extension when importing
        'import/extensions': ['error', 'always', {
            'js': 'never',
            'vue': 'never'
        }],
        // allow debugger during development
        'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0
    }
}
