/**
 * Created by Jackie on 2016/11/16.
 */
import Vue from 'vue'

function filter() {
    /*
    * 日期格式化
    * 依赖 moment
    * npm install moment --save
    *
    * import filter from './filter/filter'
    * filter()
    *
    * */
    Vue.filter('moment', (value, formatString) => {
        formatString = formatString || 'YYYY-MM-DD HH:mm:ss'
        return moment(value).format(formatString)
    })


}

export default filter
