/**
 * Created by Jackie on 2016/11/17.
 */

// import Home from './components/Home'

export default {
    '/': {
        name: 'home',
        component(resolve){
            require(['./components/Home.vue'], resolve)
        }
    },
    '/demos': {
        name: 'demos',
        component(resolve){
            require(['./components/Demos.vue'], resolve)
        }
    }
}