/**
 * Created by Jackie on 2016/11/16.
 */
import Vue from 'vue'
import Validator from 'vue-validator'

Vue.use(Validator)

// Add Validate guide
Vue.validator('phone', (value) => {
    return /^((0\d{2,3}-\d{7,8})|(1[3584]\d{9}))$/.test(value)
})
