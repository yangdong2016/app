/**
 * Created by Jackie on 2016/11/17.
 */
import Vue from 'vue'

const HTTP_REMOTE = 'http://127.0.0.1:8080'

export default {
    list(){
        return Vue.resource(`${HTTP_REMOTE}/static/info.json`)
    }

}